﻿using NewTalents.Console.Services;

namespace NewTalents.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculadora = new Calculadora();
            var resultado = calculadora.Somar(2, 2);

            System.Console.WriteLine(resultado);

            System.Console.WriteLine("Histórico");

            foreach (var item in calculadora.Historico())
            {
                System.Console.WriteLine(item);
            }
        }
    }
}
