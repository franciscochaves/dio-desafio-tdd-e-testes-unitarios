using System;
using System.Collections.Generic;

namespace NewTalents.Console.Services
{
    public class Calculadora
    {
        private List<string> _listaHistorico;

        public Calculadora()
        {
            _listaHistorico = new List<string>();
        }

        public int Somar(int num1, int num2)
        {
            var resultado = num1 + num2;

            _listaHistorico.Insert(0, $"Res: {resultado} - Data: {DateTime.Now}");

            return resultado;
        }

        public int Subtrair(int num1, int num2)
        {
            var resultado = num1 - num2;

            _listaHistorico.Insert(0, $"Res: {resultado} - Data: {DateTime.Now}");

            return resultado;
        }

        public int Multiplicar(int num1, int num2)
        {
            var resultado = num1 * num2;

            _listaHistorico.Insert(0, $"Res: {resultado} - Data: {DateTime.Now}");

            return resultado;
        }

        public int Dividir(int num1, int num2)
        {
            var resultado = num1 / num2;

            _listaHistorico.Insert(0, $"Res: {resultado} - Data: {DateTime.Now}");

            return resultado;
        }

        public List<string> Historico()
        {
            if (_listaHistorico.Count >= 3)
            {
                _listaHistorico.RemoveRange(3, _listaHistorico.Count - 3);
            }

            return _listaHistorico;
        }
    }
}
