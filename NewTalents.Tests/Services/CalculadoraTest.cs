using Xunit;
using NewTalents.Console.Services;
using System;
using System.Threading;

namespace NewTalents.Tests.Services
{
    public class CalculadoraTest
    {
        private readonly Calculadora _calculadora;

        public CalculadoraTest()
        {
            _calculadora = new Calculadora();
        }

        [Theory]
        [InlineData(1, 2, 3)]
        [InlineData(3, 4, 7)]
        [InlineData(5, 6, 11)]
        public void TesteSomar(int num1, int num2, int resultado)
        {
            var resultadoCalculadora = _calculadora.Somar(num1, num2);

            Assert.Equal(resultado, resultadoCalculadora);
        }

        [Theory]
        [InlineData(1, 2, -1)]
        [InlineData(4, 4, 0)]
        [InlineData(7, 6, 1)]
        public void TesteSubtrair(int num1, int num2, int resultado)
        {
            var resultadoCalculadora = _calculadora.Subtrair(num1, num2);

            Assert.Equal(resultado, resultadoCalculadora);
        }

        [Theory]
        [InlineData(2, 2, 4)]
        [InlineData(3, 4, 12)]
        [InlineData(5, 10, 50)]
        public void TesteMultiplicar(int num1, int num2, int resultado)
        {
            var resultadoCalculadora = _calculadora.Multiplicar(num1, num2);

            Assert.Equal(resultado, resultadoCalculadora);
        }


        [Theory]
        [InlineData(4, 2, 2)]
        [InlineData(9, 3, 3)]
        [InlineData(10, 5, 2)]
        public void TesteDividir(int num1, int num2, int resultado)
        {
            var resultadoCalculadora = _calculadora.Dividir(num1, num2);

            Assert.Equal(resultado, resultadoCalculadora);
        }

        [Fact]
        public void TestarDivisaoPorZero()
        {
            Assert.Throws<DivideByZeroException>(
                () => _calculadora.Dividir(3, 0)
            );
        }

        [Fact]
        public void TestarHistorico()
        {
            _calculadora.Somar(1, 2);
            _calculadora.Somar(2, 8);
            _calculadora.Somar(3, 7);
            _calculadora.Somar(4, 1);

            var lista = _calculadora.Historico();

            Assert.NotEmpty(lista);
            Assert.Equal(3, lista.Count);
        }
    }
}
